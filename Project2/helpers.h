/* 
 * File:   helpers.h
 * Author: cis415
 *
 * Created on May 16, 2014, 3:30 PM
 */
#ifndef HELPERS_H
#define HELPERS_H


#include <stdio.h>
#include <stdlib.h>
#include <string.h>



int str_length(char * input_string);
void copy_str(char *source, char *destination);
void prompt();
void executeJobs(int readIn, int readOut, char* arg[], int bg);
void executePipe(int readIn, int readOut, char* arg[], char*arg2[], int bg);
void my_print(char *s);
int exponent_ten(int exponent);
void remove_trailing_newline_from_string(char* string_input);
int my_atoi(char* s);



#endif 