#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>
#include <unistd.h>
#include "helpers.h"
#define BUFFER_SIZE 
#define RETURN_VALUE_ERROR -1

////////////////////////////////////////////////////////////////////////////////

void prompt(){
   
    char msg[] = "Klingon:";
    write(1, msg, sizeof(msg));
   
}

////////////////////////////////////////////////////////////////////////////////
void executePipe(int readIn, int readOut, char* arg[], char*arg2[], int bg){
    pid_t pid;
    int fd[2];
    int status;
    pipe(fd);
    pid = fork();
    if(pid == 0) {
        if(readIn != -1){
        dup2(readIn, STDIN_FILENO);
        }
        
        if(readOut != -1){
        dup2(readOut, STDOUT_FILENO);
        }
        
        execvp(arg[0], arg);
        //exit(1);
        
    }
    if (pid == -1){
        perror("fork");
        exit(1);
    }
    else {
        wait(&status);
        if(readIn != -1){
        close(readIn);
        }
        
        close(readOut);
     
    }
    pid = fork();
    if(pid == 0) {
        if(readIn != -1){
        dup2(readIn, STDIN_FILENO);
        }
        
        if(readOut != -1){
        dup2(readOut, STDOUT_FILENO);
        }
        
        execvp(arg2[0], arg2);
        //exit(1);
        
    }
    if (pid == -1){
        perror("fork");
        exit(1);
    }
    else {
        wait(&status);
        if(readIn != -1){
        close(readIn);
        }
        
        close(readOut);
     
    }
    
    
}
////////////////////////////////////////////////////////////////////////////////
void executeJobs(int readIn, int readOut, char* arg[], int bg){
    pid_t pid;
    int status;
    

    
    pid = fork();
    if(pid == 0) {
        if(readIn != -1){
        dup2(readIn, STDIN_FILENO);
        }
        
        if(readOut != -1){
        dup2(readOut, STDOUT_FILENO);
        }
        
        execvp(arg[0], arg);
        //exit(1);
        
    }
    if (pid == -1){
        perror("fork");
        exit(1);
    }
    else {
        if(readIn != -1){
        close(readIn);
        }
        
        close(readOut);
            
    }
    
    
}
////////////////////////////////////////////////////////////////////////////////
int str_length(char * input_string)
{
  if (input_string == NULL)
  {
    return 0;
  }
  int i = 0;
  for (i = 0; i < (BUFFER_SIZE + 1); i++)
  {
    if (input_string[i] == 0)
    {
      break;
    }
  }
  if (i == BUFFER_SIZE + 1)
  {
    return 0;
  }
  return i;
}
////////////////////////////////////////////////////////////////////////////////
void copy_str(char *source, char *destination){
    int i;
    for (i=0; source[i] != NULL; ++i){
        destination[i] = source[i];
    }
    destination[i] = '\0';
    return;
}
////////////////////////////////////////////////////////////////////////////////
void my_print(char *s)
{
    int string_length = str_length(s);
    int return_value =0;
    
    if (string_length == RETURN_VALUE_ERROR)
    {
        return_value = write(STDOUT_FILENO, s, string_length);
        
        if (return_value = RETURN_VALUE_ERROR)
        {
            //my_perror("write error");)
        }
        return;
                
    }
    return_value = write(STDOUT_FILENO, s, string_length);
    
    if (return_value == RETURN_VALUE_ERROR)
    {
        //perror("write error");
    }
}
////////////////////////////////////////////////////////////////////////////////
int exponent_ten(int exponent)
{
    int return_value = 1;
    if (exponent < 0)
    {
        return 0;  
    }
    if (exponent > 9)
    {
        return 0;
        
    }
    int i = 0;
    for (i = 0; i < exponent; i++)
    {
        return_value = return_value * 10;
    }
    return return_value;
}
////////////////////////////////////////////////////////////////////////////////
void remove_trailing_newline_from_string(char* string_input)
{
    int string_length = str_length(string_input);
    
    if (string_length == RETURN_VALUE_ERROR)
    {
        my_print("strLength error\n");
    }
    else
    {
        // Remove trailing \n
        
        if (string_input[string_length -1] == '\n')
        {
            string_input[string_length -1] =0;
            string_length--;
        }
    }
}
////////////////////////////////////////////////////////////////////////////////
int my_atoi(char* s)
{
    int i = 0;
    int return_value = 0;
    remove_trailing_newline_from_string(s);
    
    int string_length = str_length(s);
    
    if (string_length == RETURN_VALUE_ERROR)
    {
        return RETURN_VALUE_ERROR;
        
    }
    for (i = (string_length -1); i >= 0; i--)
    {
        if ((s[i] < '0' || s[i] > '9'))
        {
            return RETURN_VALUE_ERROR;
        }
        
    }
}
