/*
 * File:   main.c
 * Author: Cory Olivieri
 *
 * Created on April 15, 2014, 2:19 PM
 */

#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>

#include "tokenizer.h"
#include "helpers.h"

pid_t child = -1;
int alrmTrig = 0;


//prompt the user sees when they start the shell and run commands

int main(int argc, char **argv, char *envp[]) {
    int inRead;
    char buff[1024];
    int status;
    int bg;
    TOKENIZER *tokenizer;
    char *token;
    char *outFile = NULL;
    char *inFile = NULL;
    int savedOut;
    int readOut = -1;
    int readIn = -1;
    int havePipe;
    int j;
    
    char* arg[1024];
    char* arg2[1024];
   
    
    while(1) {
        
        close(readIn);
        close(readOut);
        int i = 0;
        prompt();
        while(i < 1024){
            buff[i]= 0;
            i++;
        }

        //reads in the user input from the command line
        inRead = read( STDIN_FILENO, buff, sizeof(buff) );    
        buff[inRead-1] = '\0';
        tokenizer = init_tokenizer(buff);
        i=0;
        while((token = get_next_token(tokenizer)) != NULL){
            
            if (token[0] == '<'){
                free(token);
                if((token = get_next_token(tokenizer)) != NULL){
                    inFile = (char *) malloc(str_length(token) * sizeof (char)); 
                    copy_str (token, inFile );
                    printf("infile   %s\n", inFile);
                    free(token);
                }
            }
            if (token[0] == '>'){
                free(token);
                if((token = get_next_token(tokenizer)) != NULL){
                    outFile = (char *) malloc(str_length(token) * sizeof (char)); 
                    copy_str (token, outFile );
                    printf("outfile  %s\n", outFile);
                    free(token);
                }
            }
            if (token[0] == '|'){
                free(token);
                while((token = get_next_token(tokenizer)) != NULL){     
                        if (token[0] == '<'){
                            free(token);
                            if((token = get_next_token(tokenizer)) != NULL){
                                inFile = (char *) malloc(str_length(token) * sizeof (char)); 
                                copy_str (token, inFile );
                                printf("infile   %s\n", inFile);
                                free(token);
                            }
                        }
                        if (token[0] == '>'){
                            free(token);
                            if((token = get_next_token(tokenizer)) != NULL){
                                outFile = (char *) malloc(str_length(token) * sizeof (char)); 
                                copy_str (token, outFile );
                                printf("outfile  %s\n", outFile);
                                free(token);
                            }
                        }
                        else {
                          arg2[j] = token;
                          printf("arg2    %s\n", arg2[j]);
                          j++;
                        }
                    
                }
                executePipe(readIn, readOut, arg, arg2, bg);
            }
            if (token[0] == '&'){
                bg = 1;
            }
            else {
                arg[i] = token;
                i++;
            }
        }
        if (inFile != NULL){
            readIn = open((char *) inFile, O_RDONLY);
            if (readIn == -1){
                printf("cats \n");
            }
                
        }
        if(outFile != NULL){
            readOut = open((char *) outFile, O_WRONLY | O_CREAT, 0644);
            if (readOut == -1){
                printf("cats 22 \n");
            }
                
        }
        printf("args before it goes to executeJobs  %s\n", arg[0]);
        printf("args before it goes to executeJobs  %s\n", arg[1]);
        printf("args before it goes to executeJobs  %s\n", arg[2]);
        executeJobs(readIn, readOut, arg, bg);
        dup2(savedOut, 1);
        close(savedOut);
        
        free_tokenizer(tokenizer);
    }
          

    return (0);
}

